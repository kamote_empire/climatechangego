package hack.org.arlocationbased;

import android.location.Location;

/**
 * Created by Joseph Cajida on 9/17/2016.
 */
public interface OnLocationChangedListener {
    void onLocationChanged(Location currentLocation);
}
