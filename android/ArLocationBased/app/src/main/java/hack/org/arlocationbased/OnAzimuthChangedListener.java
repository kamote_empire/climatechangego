package hack.org.arlocationbased;

/**
 * Created by Joseph Cajida on 9/17/2016.
 */
public interface OnAzimuthChangedListener {
    void onAzimuthChanged(float azimuthFrom, float azimuthTo);
}
