package hack.org.arlocationbased;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by Joseph Cajida on 9/17/2016.
 */
public class MyCurrentLocation implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    private OnLocationChangedListener onLocationChangedListener;

        public MyCurrentLocation(OnLocationChangedListener onLocationChangedListener) {
            this.onLocationChangedListener = onLocationChangedListener;
        }

        // google services library to get latitude and longitude of the device.
        protected synchronized void buildGoogleApiClient(Context context) {
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

            mLocationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                    .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                    .setFastestInterval(1 * 1000); // 1 second, in milliseconds
        }

        public void start(){
            mGoogleApiClient.connect();
        }

        public void stop(){
            mGoogleApiClient.disconnect();
        }
        @Override
        public void onConnected(Bundle bundle) {

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            Log.d("MyCurrentLocation", String.valueOf(mLastLocation));

            if (mLastLocation != null) {
                onLocationChangedListener.onLocationChanged(mLastLocation);
            }
        }

        @Override
        public void onConnectionSuspended(int i) {

        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            Log.e("MyLocation", "Location services connection failed with code " + connectionResult.getErrorCode());
        }

    // Get the last location of the device based on gps and compass
        @Override
        public void onLocationChanged(Location location) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            Log.d("LastLocation", String.valueOf(mLastLocation));
            if (mLastLocation != null) {
                Log.d("LastLocationNot", String.valueOf(mLastLocation));
                onLocationChangedListener.onLocationChanged(mLastLocation);
            }
        }

}
